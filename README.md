# CRUD users

![](https://gitlab.com/ph31/crud_users/-/raw/main/docs/detail.png)


This project is running with Php version 7.24.26

## Dependencies

1. Wampserver version 2.2.6
2. Php version 7.24.26
3. Apache version 2.4.71