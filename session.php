<?php 

session_start();

class User{

    public $name = "";
    public $last_name = "";

    function set_name($name) {
        $this->name = $name;
    }

    function set_last_name($last_name) {
        $this->last_name = $last_name;
    }

    function get_name() {
    return $this->name;
    }

      function get_last_name() {
        return $this->last_name;
      }

}


$user = new User();
$user->set_name('Jeison');
$user->set_last_name('Cortes');

$persistent = [$user];

$_SESSION['users'] = $persistent;

// echo $_SESSION['users'];

var_dump($_SESSION['users']);


?>