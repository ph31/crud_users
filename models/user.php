<?php 

class User{

    public $id;
    public $name;
    public $last_name;
    public $date_created;


    function set_id($id) {
        $this->id = $id;
    }

    function set_name($name) {
        $this->name = $name;
    }

    function set_last_name($last_name) {
        $this->last_name = $last_name;  
    }

    function set_date_created($date_created) {
        $this->date_created = $date_created;  
    }


    function get_id() {
        return $this->id;
    }

    function get_name() {
        return $this->name;
    }

    function get_last_name() {
        return $this->last_name;
    }

}


?>