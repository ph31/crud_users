
<!--Edit-->

<?php 
    include_once 'includes/header.php';   
    require_once("models/user.php");
    session_start();   

    $is_valid = false;
    $user;
    
    if (isset($_SESSION["users"]) && isset($_GET['index'])){

        $index = $_GET['index'];
        $list_users = $_SESSION["users"];

        if (!isset( $list_users[$index] )) {
            header("Location: index.php");
        } else {
            $is_valid = true;
            $user = $list_users[$index];            
        }
    
    }

    // update
    if ( isset($_POST['update'])){

        $list_users = $_SESSION["users"];

        $list_users[$index]->name = $_POST['name'];
        $list_users[$index]->last_name = $_POST['last_name'];

        $_SESSION["users"] = $list_users;
        header("Location: index.php");
    }

 

?>

    <?php if($is_valid) : ?>
        <div class="container mt-4">
            <h2 class="h5 m-4">Editar usuario</h2>
            <form action="edit.php?index=<?php echo $index?>" method="post">
                    <div class="form-group mb-4  d-flex flex-column gap-4">
                        <input  required type="text" 
                                value=<?php echo $user->name; ?>
                                class="form-control" 
                                name="name" placeholder="Nombre">
                        <input required type="text" 
                                value=<?php echo $user->last_name; ?>
                                class="form-control" name="last_name" 
                                placeholder="Apellido">
                    </div>
            
                    <button class="w-100 btn btn-success btn-sm" 
                            name="update"
                           type="submit">Editar usuario</button>
            </form>
        </div>

    <?php endif; ?>

    



<?php include_once 'includes/footer.php' ?>