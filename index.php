
<?php 
    include_once 'includes/header.php';      
    require_once("models/user.php");
    session_start();
?>

<div class="container d-flex flex-wrap align-items-center justify-content-around gap-4 mt-4">

    <!--Create-->
    <form action="save_user.php" method="post">
        <div class="form-group mb-4  d-flex flex-column gap-4">
            <input required type="text" class="form-control" id="staticEmail" name="name" placeholder="Nombre">
            <input required type="text" class="form-control" name="last_name" placeholder="Apellido">
        </div>

        <button class=" w-100 btn btn-success btn-sm" type="submit" name="save_user">Crear usuario</button>
    </form>


    <!-- Table -->
    <?php if(isset($_SESSION['users']) && count($_SESSION['users']) > 0) : ?>
        <div class="table-responsive">

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Fecha Creacion</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                        $list_users = [];
                        if (isset($_SESSION['users'])){
                            $list_users = $_SESSION['users'];
                        }

                        foreach( $list_users as $key=>$user) { ?>
                        <tr>
                            <th><?php echo $user->id; ?></th>

                            <td><?php echo $user->name; ?></td>
                            <td><?php echo $user->last_name; ?></td>
                            <td><?php echo $user->date_created; ?></td>
                            <td>
                                <a href="edit.php?index=<?php echo $key?>">
                                    <button class="btn btn-sm btn-warning">Editar</button>
                                </a>

                                <a href="delete.php?index=<?php echo $key?>">
                                    <button class="btn btn-sm btn-danger">Eliminar</button>
                                </a>
                            </td>
                        </tr>

                    <?php } ?>
        
                </tbody>
            </table>
            
        </div>

    <?php else : ?>
        <div class="alert alert-warning text-center">No hay usuarios registrados!</div>
    <?php endif; ?>
    
</div>

<?php include_once 'includes/footer.php' ?>

