<?php

    require_once ("models/user.php");
    session_start();

    $list_users = [];

    if (isset($_SESSION['users'])){
        $list_users = $_SESSION['users'];
    }


    // issset => if exist
    if ( isset($_POST['save_user']) ){
         // create user
        $user = new User();
        $user->set_id(uniqid());
        $user->set_name($_POST['name']);
        $user->set_last_name($_POST['last_name']);
        $user->set_date_created(date("Y-m-d H:i:s"));


        // save session
        $list_users[] = $user;
        $_SESSION['users'] = $list_users;

        
        // redirect
        header("Location: index.php");
    }

   

?>